<img src=".gitlab/media/logo.de.png"  width="220" height="70">

<a href="https://evoting-community.post.ch/en" title="Made with love by swisspost"><img src="https://img.shields.io/badge/Made%20with%20❤%20by-swisspost-FFCC00?style=for-the-badge"></a>

# Disclosure of the Swiss Post e-voting system: welcome!

As part of the e-voting community programme, Swiss Post is disclosing the source code, specifications and documentation of the e-voting system. The aim is to make access to the system as simple as possible for independent experts and to constantly improve the system. Disclosure facilitates an in-depth examination and dialogue between specialists and with the Swiss Post e-voting team. Swiss Post is gradually developing the system further, taking confirmed findings into consideration. Disclosure therefore serves to enhance the security of the e-voting system.

All information on the community programme is available on our [community website.](https://evoting-community.post.ch/en)

# Disclosure in stages

The e-voting system is being disclosed in stages, starting with providing the protocol for testing. Swiss Post will subsequently publish further elements of the system. Information to support the disclosure process can be found on the website. All technical data and documents are published on the GitLab platform.

The [Code of Conduct](https://gitlab.com/swisspost-evoting/documentation/-/blob/master/CONTRIBUTING.md) is important to us. It allows us to move forward together.

# Submission of findings

You can send us your findings directly on Gitlab or via the contact form. Please read up on the various options [here.](https://gitlab.com/swisspost-evoting/documentation/-/blob/master/REPORTING.md) <br>
Please comply with our [Code of Conduct.](https://gitlab.com/swisspost-evoting/documentation/-/blob/master/CONTRIBUTING.md) For confirmed critical vulnerabilities, we welcome the submission of a CVE and will support the person submitting the report.

# Coordinated Vulnerability Disclosure

In order to ensure the stability and security of the e-voting system, we have opted for coordinated vulnerability disclosure (CVD). You can support this process as follows:

- Anyone is entitled to publish their findings: in the event of critical findings, please give us up to 90 days to analyze your report. As soon as our analysis is complete, we will give the green light for publication.

- Swiss Post regularly publishes findings in a transparent manner on its website. The person reporting a finding will be acknowledged, but can also opt to stay anonymous.

# FAQ

Answers to frequently asked questions and definition of terms<br>
<a href="https://evoting-community.post.ch/en/help-and-contact/faq" title="Frequently Asked Questions"><img src="https://img.shields.io/badge/FAQ-lightblue?style=for-the-badgeand"></a>

# Support & contact

If you have any questions or require help, please do not hesitate to [contact](https://evoting-community.post.ch/en/help-and-contact/support) us.
Our specialists will be pleased to answer your questions regarding cryptography, security, documentation, the Code of Conduct or any other subject relating to Swiss Post’s e-voting-system.

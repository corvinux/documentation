# How to Report

If you have any questions or require help, please do not hesitate to contact us with the options below!

Reports are analysed in detail by our specialists and help us to improve the e-voting system. We make it our business to check and respond to every finding quickly.

Please comply with our [Code of Conduct](https://gitlab.com/swisspost-evoting/documentation/-/blob/master/CONTRIBUTING.md). For confirmed critical vulnerabilities, we welcome the submission of a CVE and support the person submitting the report. There is no remuneration for reports.

## Introduction

The following information provides an overview on the options for reporting findings or asking questions to jointly develop ideas, solve problems and plan the work ahead.

Common use cases include:

- Discussing the implementation of a new idea
- Tracking tasks and work status
- Accepting feature proposals, questions, support requests, or bug reports
- Elaborating on new code implementations

## Defining severity

The relevance of an incident can be classified as critical, high, medium or low. A description of the classification of incidents can be found below:

- **Critical**: report with critical or blocking issue
- **High**: report with urgent priority, important and time-critical finding
- **Medium**: report with standard priority, important finding
- **Low**: report with no impact on functionality or data (e.g. layout errors or spelling mistakes)


## Reporting

There are two possible means of sending us a report: 
- <a href="https://gitlab.com/swisspost-evoting/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" title="New issue"><img src="https://img.shields.io/badge/GitLab-lightblue?style=for-the-badgeand"></a><br>
For the quickest answers and feedback, we recommend that you use the implemented Gitlab issues if you have any questions or need help from a specialist.<br>
Please write your enquiry in the issue section and set the matching labels as described below. <br> 

- <a href="https://evoting-community.post.ch/en/findings/reporting-a-finding" title="New issue"><img src="https://img.shields.io/badge/Online%20form-lightblue?style=for-the-badgeand"></a><br>
Another way to submit your issue is with an online form, please fill out all the sections of the form as precise as you can and set your estimated criticallity-level.

You are free to set a report as confidential with a checkbox below each report-option if you only want to share your findings with the Swiss Post.<br> **However, if the criticality-level is set to high or critical, the "confidential"-checkbox below must be checked!**

## Report findings on GitLab

### Templates

On Gitlab, the use of description templates makes the process of reporting findings much easier. Users that create a new issue can select a description template to help them communicate with other contributors effectively.

<img src=".gitlab/media/newissue.PNG"  width="952" height="630">

To do so, please click on the drop-down Option and **choose a template** that is best suited for your issue.

<img src=".gitlab/media/support_issue.PNG"  width="540" height="249">

- For security and bug-related issues, we advise you to choose the **Security and Bugs** template
- For improvements or feature requests, please choose the **Improvements** template
- If you have any questions, please use the **Questions** template with matching labels

After you have chosen a matching template, you will see the description and examples of what to write in which section of the template.

<img src=".gitlab/media/newissueloaded.PNG"  width="786" height="451">

## Additional information & support

If you have any other unanswered questions or just want to find out more about Gitlab, please visit<br>
<a href="https://docs.gitlab.com/ee/user/project/issues/" title="GitLab Docs"><img src="https://img.shields.io/badge/GitLab_Docs-lightblue?style=for-the-badgeand"></a> 

If you want more information on e-voting in Switzerland, please visit<br>
<a href="https://evoting-community.post.ch/en/about-e-voting" title="E-voting in Switzerland"><img src="https://img.shields.io/badge/Evoting%20in%20Switzerland-FFCC00?style=for-the-badgeand"></a>

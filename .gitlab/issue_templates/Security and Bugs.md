## Brief Description/ Summary

<!-- A clear and concise description of what the bug/issue is and the precise location in the ECP repository -->


## Steps to Reproduce the Problem

<!-- How to reproduce said issue in steps(as minimally and precisely as possible) -->

1.
2.
3.
4.


## Expected Behavior

<!-- A clear and concise description of what you expected to happen -->


## Actual Behavior

<!-- A clear and concise description of the observed behavior -->


## Impact

<!-- Is the issue a minor or major one, maybe even blocking the process? -->


## Severity

<!-- Please add a severity level according to the Scala critical / high / medium / low -->

<!-- critical = report with critical or blocking issue -->
<!-- high     = report with urgent priority, important and time-critical finding -->
<!-- medium   = report with standard priority, important finding -->
<!-- low      = report with no impact on functionality or data (e.g. layout errors or spelling mistakes) -->

<!-- If the severity is high or critical, the "confidential"-checkbox below must be checked! -->


## Possible Solution

<!-- Maybe you have an Idea on how to solve said issue? Let us know! -->
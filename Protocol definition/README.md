Accompanying documentation for *Swiss Post Voting Protocol - Computational Proof of Complete Verifiability and Privacy*

# Definition

## What is the content of this document?

This document describes the cryptographic building blocks and shows how they work together to ensure verifiability and vote privacy. Moreover, it provides a mathematical proof that the cryptographic protocol achieves the desired security objectives under a minimal set of assumptions. The cryptographic proof is a central element of guaranteeing the security of a protocol in modern cryptography.

## What is a protocol?

A [security protocol](https://en.wikipedia.org/wiki/Cryptographic_protocol) (cryptographic protocol or encryption protocol) is an abstract or concrete protocol that performs a security-related function and applies cryptographic methods, often as sequences of cryptographic primitives.

## Why is a cryptographic proof important?

In line with the current best practice in cryptography, the Federal Chancellery requires a cryptographic proof of verifiability and privacy. To cite [Katz and Lindell](http://crypto.fmf.ktu.lt/lt/xdownload/2008%20-%20Jonathan%20Katz,%20Yehuda%20Lindell%20-%20Introduction%20to%20modern%20cryptography.pdf): *Without proof that no adversary of the specified power can break the scheme, we are left only with our intuition that this is the case. Experience has shown that intuition in cryptography and computer security is disastrous. There are countless examples of unproven schemes that were broken, sometimes immediately and sometimes years after being presented or deployed.*

# Changes since publication in 2019

Since its initial publication in 2019, we have improved our cryptographic protocol to ensure that it is more:

- complete: we have significantly expanded the description of the underlying building blocks (mix net, zero-knowledge proofs, etc.).

- understandable: we have simplified the description of the protocol and aligned key, variable and algorithm names.

- rigorous: we have improved the coherence of the theorems and proofs; for instance, by taking into account multiple voting options.

Furthermore, we have merged in one single document the proofs of complete verifiability and vote secrecy.

# Known open issues

In future versions, we plan to address the following issues: 
	
- Currently, our system encrypts votes with a 2048-bit El Gamal key. While some standards deem 2048 bits sufficient, other standards advocate the use of 3072 bits. We plan to increase the key size to 3072 bits.

- Cryptographic proofs deal with abstractions of the actual implementation. Explaining and justifying these abstractions is an essential element. We plan to provide further justification regarding issues such as authentication and certain edge cases in future versions of the document.

- Proving security properties involves complex mathematics. Therefore, cryptographic proofs must provide clear and detailed explanations of each step, helping reviewers to understand and scrutinize the security of the protocol. We plan to iteratively improve the clarity and level of detail of the security analysis.

- While MGF1 (Mask generation function) is standard and described in RFC2437, its usage as a key-derivation function is not common in cryptographic protocols. We plan to replace it with a more standard key derivation function in future versions of the protocol. 

# Limitations

Our cryptographic proof demonstrates that the Swiss Post voting protocol guarantees complete verifiability and vote privacy.

However, no cryptographic protocol is unconditionally secure. The Swiss Post voting protocol bases its security objectives on a few assumptions. Even though these assumptions comply with the [Federal Chancellery's Ordinance on Electronic Voting](https://www.admin.ch/opc/en/classified-compilation/20132343/index.html), we will highlight some of them here for the sake of clarity and transparency:

- First, we are defending our security properties against an adversary that is polynomially bounded. An efficient, fault-tolerant quantum computer could break some of the mathematical assumptions underpinning the Swiss Post voting protocol. Quantum-resistant e-voting protocols exist, but they are relatively recent, and their security properties are less understood. We consider quantum-resistance an area for future improvement of the protocol. [The Federal Chancellery's *Summary of the Expert Dialog 2020*, section 11.2.11 addresses this topic](https://www.newsd.admin.ch/newsd/message/attachments/63915.pdf).

- Second, for the purpose of providing vote secrecy, the Swiss Post voting protocol assumes that the attacker is not controlling the voting client. While we guarantee individual verifiability even if the voting client is malicious, we cannot use cryptography to prevent attackers from spying on the voter’s choices on malicious clients. However, the Swiss Post voting protocol prevents the server from breaking vote secrecy if at least one control component is honest.

- Third, the Swiss Post voting protocol assumes a trustworthy print office. The print office generates keys and codes in conjunction with the control components and sends the voters’ voting cards. By its nature, the print office handles sensitive data, and the process for generating and sending voting cards must meet [rigorous requirements](https://www.bk.admin.ch/dam/bk-intra/de/dokumente/pore/politische_rechte/anforderungskatalogdruckereien.pdf.download.pdf/anforderungskatalogdruckereien.pdf). In general, e-voting systems aim to distribute trust and detect any attack or malfunction if at least one control component works correctly. However, we believe that there are certain limits for distributing the functionality of the print office: one cannot expect the voter to combine different code sheets by hand, and the costs of printing multiple code sheets in independent printing facilities would be currently prohibitive. For now, we propose to implement a return code scheme with a single print office, which must put organizational and technical means into place to prevent leaking the secret codes.
  * The reduction of trust assumptions in the print office is part of the [final report of the e-voting steering committee of cantons and confederation for the revision of the current conditions, Measure A.5](https://www.newsd.admin.ch/newsd/message/attachments/64680.pdf).


# Public scrutiny

Swiss Post is developing the e-voting system independently, but not alone. On the contrary we are increasingly collaborating with experts from academia and industry. 

After the initial publication, numerous researchers submitted and published valuable reports on our cryptographic protocol. Naming every single contribution would be beyond the scope of this document. However, we plan to publish a detailed response to the following two outstanding articles:

-	[Analysis of the Cryptographic Implementation of the Swiss Post Voting Protocol, P. Locher, R. Haenni, R. Koenig, 2019](https://www.bk.admin.ch/dam/bk/de/dokumente/pore/E-Voting_Report_Locher_Haenni_Koenig_Juli%202019.pdf.download.pdf/E-Voting_Report_Locher_Haenni_Koenig_Juli%202019.pdf)
-	[How not to prove your election outcome, T. Haines, S. Lewis, O. Pereira and V. Teague, 2020](https://dial.uclouvain.be/pr/boreal/object/boreal%3A223906/datastream/PDF_01/view)

We thank all researchers for their valuable suggestions and critiques, and we hope that they continue to scrutinize the Swiss Post Voting Protocol.